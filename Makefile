TEX:=$(wildcard *.tex)

all: ${TEX:%.tex=pdflatex_%.pdf} ${TEX:%.tex=lualatex_%.pdf} ${TEX:%.tex=xelatex_%.pdf}

pdflatex_%.pdf: %.tex
	pdflatex -jobname pdflatex_$* $<
	pdflatex -jobname pdflatex_$* $<
	pdflatex -jobname pdflatex_$* $<

lualatex_%.pdf: %.tex
	lualatex -jobname lualatex_$* $<
	lualatex -jobname lualatex_$* $<
	lualatex -jobname lualatex_$* $<

xelatex_%.pdf: %.tex
	xelatex -jobname xelatex_$* $<
	xelatex -jobname xelatex_$* $<
	xelatex -jobname xelatex_$* $<
